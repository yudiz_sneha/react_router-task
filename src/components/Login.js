import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import {
    Button,
    TextField,
    Grid,
    } from "@material-ui/core";
    import './Login.css'

const Login = () => {
	const token = "Login";
	const navigate = useNavigate();
	const login = () => {
		localStorage.setItem(token, "Successfully Logged In.");
		navigate("/");
	};

	return (
        <Grid item>
		<form
			className="login"
			onSubmit={(e) => {
				e.preventDefault();
				login();
			}}
		>
			
            <div className="Login">
                <Grid item>
                <div className="text">Login</div>
					<TextField
                        color="primary"
						type="email"
						className=""
						required
						name="email"
						placeholder="Email"
                        margin="normal"
                        variant="outlined"
                    
                        inputProps={{ style: { color: 'white' }}}
					/>
                </Grid>
                <Grid item>
					<TextField
                      
						type="password"
						className=""
						placeholder="Password"
						required
                        margin="normal"
                        variant="outlined"
            
                        inputProps={{ style: { color: 'white' }}}
					/>
                </Grid>
                <div className="Button">
				<Button variant="contained" color="primary" type="submit" style={ {color: "white"} }>
				    Submit
				</Button>
                </div>
                </div>
		</form>
        </Grid>
	);
};

export default Login;
