import React from "react";
import { Navigate } from "react-router-dom";
import { isLoggedIn } from "./isLoggedIn";

const PublicRoute = ({ children }) => {
	console.log(isLoggedIn());

	return isLoggedIn() === true ? <Navigate to="/" replace /> : children;
};

export default PublicRoute;
